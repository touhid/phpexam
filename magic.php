<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 
 // This prints file full path and name
 echo "This file full path and file name is '" . __FILE__ .  "" . "<br>";
 
 // This prints file full path, without file name
 echo "This file full path is " . __DIR__ . "" . "<br>";
 
 // This prints current line number on file
 echo "This is line number " . __LINE__ .  "" . "<br>";
 
 // Really simple basic test function
 function test_function_magic_constant() {
 echo "This is from '" . __FUNCTION__ . "' function.\n";
 }